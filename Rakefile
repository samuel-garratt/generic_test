# frozen_string_literal: true

require 'bundler/gem_tasks'
require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(spec: :start_website) do |task|
  task.pattern = 'spec/test_gem/*spec.rb'
end

directory 'logs'

desc 'Start virtual web service'
task start_website: :logs do
  require 'generic_test'
  mkdir_p 'logs'
  ENV['test_site_pid'] = Process.spawn('jekyll', 'serve', '-s', 'test_site',
                                       err: %w[logs/test_site.log w]).to_s
  sleep 2 # Wait a little for virtual server to start up
  puts 'Running test site on pid ' + ENV['test_site_pid']
  ENV['PAGE_URL'] = 'http://127.0.0.1:4000'
  ENV[GT_USERNAME] = 'Test User'
  ENV[GT_PASSWORD] = 'Test Password'
  ENV[GT_LOGIN_URL] = 'http://127.0.0.1:4000/login_page.html'
end

task default: :spec
