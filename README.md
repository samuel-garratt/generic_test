# GenericTest

This gem provides an easy way to run generic tests for a website/api

Test site is at [https://samuel-garratt.gitlab.io/generic_test](https://samuel-garratt.gitlab.io/generic_test)

## Quick start

Run test gem through docker against this website with (Note it is an example that has some failures)

`docker run --rm -t registry.gitlab.com/samuel-garratt/generic_test generic_test page samuel-garratt.gitlab.io/generic_test`

To get a log of your tests share a volume with the container. 

`docker run --rm -v "$PWD":/test -t registry.gitlab.com/samuel-garratt/generic_test generic_test page samuel-garratt.gitlab.io/generic_test`

> Please share this volume from an empty folder. Running from within another gem might cause an issue

To test test links on all subpages of a site add `--all-subpages` to the command. 

To test without performing spellchecking of common spelling add `--no-spellcheck`

E.g

`docker run --rm -t registry.gitlab.com/samuel-garratt/generic_test generic_test page samuel-garratt.gitlab.io/generic_test --all-subpages --no-spellcheck`

You can see all options by running

`docker run -t registry.gitlab.com/samuel-garratt/generic_test generic_test help page`

## Add to gitlab CI for your website

Add the following `YAML` to test against a website
```yaml
example_test:
  stage: test
  image: registry.gitlab.com/samuel-garratt/generic_test
  script:
    - generic_test page YOUR_WEBSITE_URL
  variables:
    GIT_STRATEGY: none # Make more efficient and remove any dependency conflicts
  artifacts:
    paths:
      - logs/*.xml
      - logs/*.html # HTML reports
    reports:
      junit: logs/*.xml
    expire_in: 1 week
    when: always
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'generic_test'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install generic_test

## Usage

### Running locally. 

Use the executable `generic_test`, add the page command and put a website to check:

`generic_test page URL_OF_WEBSITE` 

E.g.,

`generic_test page samuel-garratt.gitlab.io/generic_test` 

### Run with docker

`docker run -t registry.gitlab.com/samuel-garratt/generic_test generic_test page URL_OF_WEBSITE`

`docker run -t registry.gitlab.com/samuel-garratt/generic_test generic_test page samuel-garratt.gitlab.io/generic_test`

Check version

`docker run -t registry.gitlab.com/samuel-garratt/generic_test generic_test version`

### Through login page

Environment variables store Login URL, username and password
* GT_LOGIN_URL: Url to login at
* GT_USERNAME: Username to use
* GT_PASSWORD: Password to use

At the moment Generic test tries to be smart to identify the username, password and submit elements. In
the future, option will be given to provide your own identifiers if this does not work.

These variables can be set in `docker run` 

`docker run -e "GT_LOGIN_URL=samuel-garratt.gitlab.io/generic_test/login_page" -e "GT_USERNAME=Tester" -e "GT_PASSWORD=Pass" -v "$PWD":/test -t registry.gitlab.com/samuel-garratt/generic_test generic_test page samuel-garratt.gitlab.io/generic_test`

These variables can also be set in a `.generic_test.page.yml` (you will need a volume to have this available to
docker container)

E.g., file to login as above:

```yaml
login:
  url: 'samuel-garratt.gitlab.io/generic_test/login_page'
  username: 'Tester'
  password: 'Pass'
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update 
the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for 
the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

The pipeline running on master will deploy a new version of the docker container with the latest version released
to rubygems.org. Therefore a `rake release` is required before any changes will be reflected in the newly built
docker container.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/samuel-garratt/generic_test. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/samuel-garratt/generic_test/-/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the GenericTest project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/samuel-garratt/generic_test/blob/master/CODE_OF_CONDUCT.md).
