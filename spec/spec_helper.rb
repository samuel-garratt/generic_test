# frozen_string_literal: true

require 'bundler/setup'
require 'simplecov'
SimpleCov.start do
  add_filter 'spec'
end

SimpleCov.at_exit do
  SimpleCov.result.format!
  SimpleCov.minimum_coverage 85
end

require 'generic_test/setup'

RSpec.configure do |config|
  # Close test server after all RSpec tests have run
  config.after(:suite) do
    if ENV['test_site_pid']
      puts "Closing test site at #{ENV['test_site_pid']}"
      Process.kill(:QUIT, ENV['test_site_pid'].to_i)
    end
    GenericTest.browser&.close
  end
end
