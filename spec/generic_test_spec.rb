# frozen_string_literal: true

# Used by executable to verify a particular web page
require 'generic_test/setup'
page = GenericTest.pages.first
checkmark = "\u2713"
links = page.links
RSpec.describe "#{page.title} Links respond" do
  links.each_with_index do |link, index|
    link_text = link.text.to_s.empty? ? '' : " \"#{link.text}\""
    next if GenericTest.tested_links.include? link.href

    GenericTest.tested_links << link.href
    link_href = link.href
    it "Link (#{GenericTest.tested_links.count})#{link_text} to '#{link.href}' succeeds" do
      expect(Checker.link_status(link_href)).to be_between 200, 399
    end
  end
  puts "Checking #{GenericTest.tested_links.count} non duplicate links"
end

if GenericTest.all_subpages
  extra_subpage_links = []
  browser = GenericTest.browser
  page.internal_links.each do |href|
    browser.goto href
    sub_page = GenericTest::Page.new(browser)
    sub_page.internal_links.each do |internal_link|
      extra_subpage_links << internal_link unless page.internal_links.include? internal_link
    end
    extra_subpage_links.uniq!
    puts "Extra subpage links #{extra_subpage_links.count}"
    RSpec.describe "Subpage #{browser.title} #{href}" do
      sub_page.links.each_with_index do |link, index|
        link_text = link.text.to_s.empty? ? '' : " \"#{link.text}\""
        next if GenericTest.tested_links.include? link.href

        GenericTest.tested_links << link.href
        link_href = link.href
        it "Link (#{GenericTest.tested_links.count})#{link_text} to '#{link.href}' succeeds" do
          expect(Checker.link_status(link_href)).to be_between 200, 399
        end
      end
      puts "#{checkmark}  Checking #{GenericTest.tested_links.count} non duplicate links"
    end
  end

  extra_subpage_links.each do |href|
    browser.goto href
    sub_page = GenericTest::Page.new(browser)
    
    RSpec.describe "Subpage lv 2 #{browser.title} #{href}" do
      sub_page.links.each_with_index do |link, index|
        link_text = link.text.to_s.empty? ? '' : " \"#{link.text}\""
        next if GenericTest.tested_links.include? link.href

        GenericTest.tested_links << link.href
        link_href = link.href
        it "Link (#{GenericTest.tested_links.count})#{link_text} to '#{link.href}' succeeds" do
          expect(Checker.link_status(link_href)).to be_between 200, 399
        end
      end
      puts "#{checkmark}  Checking #{GenericTest.tested_links.count} non duplicate links (lv2)"
    end
  end
end

emails = page.emails
RSpec.describe "#{page.title} Emails are valid" do
  emails.each do |email|
    it "Email (#{email}) is valid" do
      expect(Checker.valid_email?(email)).to be nil
    end
  end
end

if ENV['SPELLCHECK_ON'] == 'true'
  RSpec.describe "#{page.title} Page content spelling" do
    spellcheck_report = SpellCheck::ProofReader.check(page.text)
    context 'does not have common spelling mistakes' do
      it 'has no mistakes' do
        expect(spellcheck_report.errata.size).to eq 0
      end
      spellcheck_report.errata.group_by(&:line_number).each do |line_number, mistakes|
        context "Line #{line_number}" do
          mistakes.each_with_index do |mistake, index|
            it "(#{index + 1}) #{mistake.pattern}" do
              expect(mistake.pattern).to eq mistake.expected
            end
          end
        end
      end
    end
  end
end
