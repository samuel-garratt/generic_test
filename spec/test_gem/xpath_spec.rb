# frozen_string_literal: true

require 'generic_test/xpath'

describe Xpath do
  context '.upcase' do
    it 'return xpath upcase function' do
      expect(Xpath.upcase('text()')).to include 'translate(text()'
    end
  end
end
