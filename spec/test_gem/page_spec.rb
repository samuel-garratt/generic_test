# frozen_string_literal: true

# Test gem works

page = GenericTest.pages.first
links = page.links

RSpec.describe 'Links' do
  it 'counts correctly (not include invalid)' do
    expect(links.size).to eq 3
  end
  context 'successful' do
    it 'external page' do
      expect(Checker.link_status(links[0].href)).to be_between 200, 399
    end
    it 'internal link' do
      expect(Checker.link_status(links[2].href)).to be_between 200, 399
    end
  end
  it 'broken causes failure' do
    puts page.links[1].text
    puts page.links[1].href
    expect(Checker.link_status(links[1].href)).to eq 404
  end
end

RSpec.describe 'Emails' do
  it 'counts correctly' do
    expect(page.emails.size).to eq 2
  end
  context 'validate' do
    it 'accepts valid email' do
      expect(Checker.valid_email?(page.emails[0])).to be nil
    end
    it 'returns invalid email error' do
      expect(Checker.valid_email?(page.emails[1])).to eq 'Domain name not registered'
    end
  end
end
