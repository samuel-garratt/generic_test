FROM ruby:3.3
LABEL AUTHOR=SamuelGarratt

# Required ruby gems
RUN mkdir /test
WORKDIR /test
RUN gem install bundler generic_test

# Google chrome
ARG CHROME_VERSION="google-chrome-stable"
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
  && apt-get update -qqy \
  && apt-get -qqy install \
    ${CHROME_VERSION:-google-chrome-stable} \
  && rm /etc/apt/sources.list.d/google-chrome.list \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# Add language settings to handle special characters
RUN export LANG=C.UTF-8
RUN export LANGUAGE=C.UTF-8
RUN export LC_ALL=C.UTF-8
# By running test image is checked and webdriver will be cached
RUN generic_test page samuel-garratt.gitlab.io/generic_test/about.html
