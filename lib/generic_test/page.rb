# frozen_string_literal: true

module GenericTest
  # Represents a web page state at a particular point of time
  class Page
    # @return [Array] List of links elements on the page
    attr_accessor :links
    # @return [Array] List of urls that include domain being tested
    attr_accessor :internal_links
    # @return [Array] List of emails
    attr_accessor :emails
    attr_accessor :url
    attr_accessor :html
    attr_accessor :text
    attr_accessor :title

    # Store state of page when it is loaded
    # @param [Watir::Browser] browser Watir browser
    def initialize(browser)
      self.emails, self.links = browser.links.partition do |link|
        link.href.start_with?('mailto:')
      end
      links.reject! { |link| link.href.empty? || link.href.start_with?('javascript:') || link.href.include?('tel:') }
      emails.collect! { |link| link.href.split(':').last }
      self.internal_links = links.filter do |link|
        link.href.include? ENV["PAGE_URL"]  
      end.collect { |link| link.href  }.uniq
      self.url = browser.url
      self.html = browser.html
      self.text = browser.text
      self.title = browser.title
      puts "Found #{links.count} links, #{emails.count} emails at #{url} (#{title})"
    end
  end
end
