# frozen_string_literal: true

# Module to help form xpaths for generic identification
module Xpath
  class << self
    # @return [String] Translate string to uppercase letters
    def upcase(xpath_method)
      "translate(#{xpath_method},'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"
    end

    # @return [String] Xpath to retrieve input beside a label
    def input_by_label(label)
      upcase_text = upcase('text()')
      "//input[@id=(//label[contains(#{upcase_text},'#{label.upcase}')]/@for)]"
    end

    # @return [String] Xpath to retrieve input either by a label or a attribute matching it
    def label_or_attribute(field_name)
      any_input_with_atr = "//input[@*='#{field_name}']"
      input_with_text = "//input[text()='#{field_name}']"
      [input_by_label(field_name), any_input_with_atr, input_with_text]
        .join('|')
    end
  end
end
