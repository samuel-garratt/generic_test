# frozen_string_literal: true

# One file to require gem and set things up, arriving at page to check
require 'generic_test'
require_relative 'xpath'

ENV['HEADLESS'] ||= 'true'
ENV['PAGE_NUM'] ||= '1'
browser_args = %w[--disable-popup-blocking --no-sandbox --disable-dev-shm-usage]
browser_args << '--headless' if ENV['HEADLESS'] == 'true'

GenericTest.browser = Watir::Browser.new :chrome, options: { args: browser_args }

raise 'PAGE_URL environment variable not set' unless ENV['PAGE_URL']

require 'yamler'
if File.exist?(GT_PAGE_SETTING_FILE) && !File.read(GT_PAGE_SETTING_FILE).empty?
  gt_page_settings = Yamler.load 'generic_test.page.yml'
  if gt_page_settings['login']
    ENV[GT_USERNAME] = gt_page_settings['login']['username']
    ENV[GT_PASSWORD] = gt_page_settings['login']['password']
    ENV[GT_LOGIN_URL] = gt_page_settings['login']['url']
  end
end

browser = GenericTest.browser
if ENV[GT_LOGIN_URL] && ENV[GT_USERNAME] && ENV[GT_PASSWORD]
  browser.goto ENV[GT_LOGIN_URL]
  browser.text_field(xpath: Xpath.label_or_attribute('username')).set ENV[GT_USERNAME]
  browser.text_field(xpath: Xpath.label_or_attribute('password')).set ENV[GT_PASSWORD]
  login_button = browser.button(xpath: "//*[@type='submit']|//*[@*='Login']")
  login_button.click
  login_button.wait_while(&:present?)
  puts "Login through #{ENV[GT_LOGIN_URL]} successful!"
end

GenericTest.all_subpages = true if ENV['ALL_SUBPAGES'] == 'true'
ENV['PAGE_URL'] = RestClient.get(ENV['PAGE_URL']).request.url # Check page exists
puts "Loading..."
browser.goto ENV['PAGE_URL']
sleep 2.5 # Give a bit of time for page to load
puts "Checking #{ENV['PAGE_URL']} (#{browser.title})"
GenericTest.pages << GenericTest::Page.new(browser)
if GenericTest.all_subpages
  puts "Subpages include #{GenericTest.pages.first.internal_links.count } links"
end
