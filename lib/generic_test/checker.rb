# frozen_string_literal: true

require 'rest-client'
require 'email_address'

# Module for checking parts on a site
module Checker
  class << self
    # Using Javascript fetch to retrieve URL. See https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch and
    # https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch#Parameters for details
    # @return [Integer] Status code
    def link_status(href)
      return unless href

      init_options = "const myInit = {
        method: 'GET',
        credentials: 'same-origin',
        cache: 'default',
        mode: 'cors'
      };"
      get_js_code = "var url='#{href}'; #{init_options} return fetch(url, myInit).then(res=>{return res.status});"
      GenericTest.browser.execute_script get_js_code
    rescue Selenium::WebDriver::Error::JavascriptError
      return call_via_rest_client(href) unless GenericTest.only_javascript

      raise GenericTest::Error, "Failed to fetch url '#{href}'"
    end

    # @return [Nil, String] Nil if valid, string if error
    def valid_email?(email_address)
      EmailAddress.error email_address
    end

    # Use rest-client to try and get status of URL
    def call_via_rest_client(href)
      RestClient.get(href).code
    rescue RestClient::Exception => e
      return e.response&.code if e.respond_to? :response

      raise e
    end
  end
end
