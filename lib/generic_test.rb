# frozen_string_literal: true

require 'generic_test/version'
require 'generic_test/page'
require 'generic_test/checker'
require 'spellcheck'
require 'watir'

# Handles testing of sites generically for common issues
module GenericTest
  class Error < StandardError; end
  @pages = []
  # @return [Boolean] Whether to only use Javascript. RestClient won't run in same context as
  # browser so less likely to be accurate for logged in pages
  @only_javascript = false

  @tested_links = []
  class << self
    attr_accessor :browser
    attr_accessor :pages
    attr_accessor :tested_links
    # @return [Boolean]
    attr_accessor :only_javascript
    # @return [Boolean] Whether to check all subpages of initial page
    attr_accessor :all_subpages
  end
end

# @return [String] Generic Test URL to login before arriving at PAGE_URL to test.
#   Needs GT_USERNAME and GT_PASSWORD to be used
GT_LOGIN_URL = 'GT_LOGIN_URL'
GT_USERNAME = 'GT_USERNAME'
GT_PASSWORD = 'GT_PASSWORD'
GT_PAGE_SETTING_FILE = 'generic_test.page.yml'
