# frozen_string_literal: true

require_relative 'lib/generic_test/version'

Gem::Specification.new do |spec|
  spec.name          = 'generic_test'
  spec.version       = GenericTest::VERSION
  spec.authors       = ['Samuel Garratt']
  spec.email         = ['samuel.garratt@integrationqa.com']

  spec.summary       = 'Generic tests for a website/api.'
  spec.description   = 'Easy way to run generic tests for a website/api.'
  spec.homepage      = 'https://gitlab.com/samuel-garratt/generic_test'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.6.0')
  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/samuel-garratt/generic_test'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/samuel-garratt/generic_test/blob/master/ChangeLog'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0")
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
  # Development dependencies are installed too since running external tests within gem
  spec.add_development_dependency 'jekyll' # For hosting test website
  spec.add_development_dependency 'rake'
  spec.add_dependency 'rspec_junit_formatter' # Needed for nicer reporting of rspec for use in CI
  spec.add_dependency 'simplecov'     # Code coverage

  spec.add_dependency 'email_address' # Verify email address
  spec.add_dependency 'rest-client'   # Verify links
  spec.add_dependency 'rspec'         # Test framework
  spec.add_dependency 'spellcheck'    # Check for common spelling errors
  spec.add_dependency 'thor'          # Command line utility
  spec.add_dependency 'watir'         # UI interaction
  spec.add_dependency 'yamler'
end
